/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./server/index.js":
/*!*************************!*\
  !*** ./server/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-config */ \"react-router-config\");\n/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _render__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./render */ \"./server/render.js\");\n/* harmony import */ var _src_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../src/store */ \"./src/store.js\");\n/* harmony import */ var _src_router_Routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../src/router/Routes */ \"./src/router/Routes.js\");\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\n\nconst PORT = process.env.PORT || 8079;\nconst app = express__WEBPACK_IMPORTED_MODULE_0___default()();\napp.use(\"/dist\", express__WEBPACK_IMPORTED_MODULE_0___default.a.static(\"dist\"));\napp.use(\"/img\", express__WEBPACK_IMPORTED_MODULE_0___default.a.static(\"img\"));\napp.get(\"/*\", async (req, res) => {\n  console.log(req.url);\n  const actions = Object(react_router_config__WEBPACK_IMPORTED_MODULE_2__[\"matchRoutes\"])(_src_router_Routes__WEBPACK_IMPORTED_MODULE_5__[\"default\"], req.path).map(({\n    route\n  }) => route.component.fetching ? route.component.fetching(_objectSpread({}, _src_store__WEBPACK_IMPORTED_MODULE_4__[\"default\"], {\n    path: req.path,\n    req\n  })) : null).map(async actions => await Promise.all((actions || []).map(p => p && new Promise(resolve => p.then(resolve).catch(resolve)))));\n  await Promise.all(actions);\n  const context = {};\n  const content = Object(_render__WEBPACK_IMPORTED_MODULE_3__[\"default\"])(req.url, _src_store__WEBPACK_IMPORTED_MODULE_4__[\"default\"], context);\n  res.send(content);\n});\napp.listen(PORT, () => console.log(`Frontend service listening on port: ${PORT}`));\n\n//# sourceURL=webpack:///./server/index.js?");

/***/ }),

/***/ "./server/render.js":
/*!**************************!*\
  !*** ./server/render.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-config */ \"react-router-config\");\n/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _src_router_Routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../src/router/Routes */ \"./src/router/Routes.js\");\n/* harmony import */ var _src_history__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../src/history */ \"./src/history.js\");\n\n\n\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ((pathname, store, context) => {\n  const content = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_1__[\"renderToString\"])(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_2__[\"Provider\"], {\n    store: store\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__[\"Router\"], {\n    history: _src_history__WEBPACK_IMPORTED_MODULE_6__[\"default\"],\n    location: pathname,\n    context: context\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, Object(react_router_config__WEBPACK_IMPORTED_MODULE_4__[\"renderRoutes\"])(_src_router_Routes__WEBPACK_IMPORTED_MODULE_5__[\"default\"])))));\n  return `\n  <!DOCTYPE html>\n      <html lang=\"en\">\n      <head>\n        <meta charset=\"UTF-8\">\n        <title>Title</title>\n      </head>\n      <body>\n      \n      <div id=\"app\">${content}</div>\n      <script>\n        window.INITIAL_STATE = ${JSON.stringify(store.getState())}\n      </script>\n      <script src=\"/dist/bundle.js\"></script>\n      </body>\n      </html>\n  `;\n});\n\n//# sourceURL=webpack:///./server/render.js?");

/***/ }),

/***/ "./src/actions/actionNames.js":
/*!************************************!*\
  !*** ./src/actions/actionNames.js ***!
  \************************************/
/*! exports provided: ROOT, CHANGE_FILTERS, CHANGE_PAGE, REQUEST_ISSUES, RECEIVE_ISSUES, REQUEST_ISSUE, RECEIVE_ISSUE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ROOT\", function() { return ROOT; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CHANGE_FILTERS\", function() { return CHANGE_FILTERS; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CHANGE_PAGE\", function() { return CHANGE_PAGE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"REQUEST_ISSUES\", function() { return REQUEST_ISSUES; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RECEIVE_ISSUES\", function() { return RECEIVE_ISSUES; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"REQUEST_ISSUE\", function() { return REQUEST_ISSUE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"RECEIVE_ISSUE\", function() { return RECEIVE_ISSUE; });\nconst ROOT = \"https://api.github.com/repos/\";\nconst CHANGE_FILTERS = \"CHANGE_FILTERS\";\nconst CHANGE_PAGE = \"CHANGE_PAGE\";\nconst REQUEST_ISSUES = \"REQUEST_ISSUES\";\nconst RECEIVE_ISSUES = \"RECEIVE_ISSUES\";\nconst REQUEST_ISSUE = \"REQUEST_ISSUE\";\nconst RECEIVE_ISSUE = \"RECEIVE_ISSUE\";\n\n//# sourceURL=webpack:///./src/actions/actionNames.js?");

/***/ }),

/***/ "./src/actions/issues.js":
/*!*******************************!*\
  !*** ./src/actions/issues.js ***!
  \*******************************/
/*! exports provided: changePage, fetchIssues, fetchIssue, changeFilters */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changePage\", function() { return changePage; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"fetchIssues\", function() { return fetchIssues; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"fetchIssue\", function() { return fetchIssue; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"changeFilters\", function() { return changeFilters; });\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ \"axios\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _actionNames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./actionNames */ \"./src/actions/actionNames.js\");\n\n\nconst changePage = page => async (dispatch, getState) => {\n  try {\n    const res = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(getState().issues.links[page]);\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUES\"],\n      payload: res\n    });\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"CHANGE_PAGE\"],\n      payload: page\n    });\n  } catch (e) {\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUES\"],\n      payload: []\n    });\n  }\n};\nconst fetchIssues = name => async (dispatch, getState) => {\n  const {\n    filter,\n    perPage,\n    page\n  } = getState().issues;\n\n  if (name) {\n    try {\n      dispatch({\n        type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"REQUEST_ISSUES\"],\n        payload: name\n      });\n      const res = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${_actionNames__WEBPACK_IMPORTED_MODULE_1__[\"ROOT\"]}${name}/issues?state=${filter}&per_page=${perPage}&page=${page}`);\n      dispatch({\n        type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUES\"],\n        payload: res\n      });\n    } catch (e) {\n      dispatch({\n        type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUES\"],\n        payload: []\n      });\n    }\n  }\n};\nconst fetchIssue = path => async dispatch => {\n  try {\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"REQUEST_ISSUE\"]\n    });\n    const res = await axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(`${_actionNames__WEBPACK_IMPORTED_MODULE_1__[\"ROOT\"]}${path}`);\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUE\"],\n      payload: res.data\n    });\n  } catch (e) {\n    dispatch({\n      type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"RECEIVE_ISSUE\"],\n      payload: {}\n    });\n  }\n};\nconst changeFilters = filter => async (dispatch, getState) => {\n  dispatch({\n    type: _actionNames__WEBPACK_IMPORTED_MODULE_1__[\"CHANGE_FILTERS\"],\n    payload: filter\n  });\n  dispatch(fetchIssues(getState().repo));\n};\n\n//# sourceURL=webpack:///./src/actions/issues.js?");

/***/ }),

/***/ "./src/components/App/index.js":
/*!*************************************!*\
  !*** ./src/components/App/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! query-string */ \"query-string\");\n/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(query_string__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _actions_issues__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../actions/issues */ \"./src/actions/issues.js\");\n/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles */ \"./src/components/styles.js\");\n/* harmony import */ var _Sidebar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Sidebar */ \"./src/components/Sidebar/index.js\");\n/* harmony import */ var _ListItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ListItem */ \"./src/components/ListItem/index.js\");\nvar _dec, _class, _temp;\n\nfunction _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\n\n\nlet App = (_dec = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__[\"connect\"])(({\n  issues\n}) => ({\n  issues\n}), {\n  fetchIssues: _actions_issues__WEBPACK_IMPORTED_MODULE_3__[\"fetchIssues\"],\n  changeFilters: _actions_issues__WEBPACK_IMPORTED_MODULE_3__[\"changeFilters\"],\n  changePage: _actions_issues__WEBPACK_IMPORTED_MODULE_3__[\"changePage\"]\n}), _dec(_class = (_temp = class App extends react__WEBPACK_IMPORTED_MODULE_0__[\"Component\"] {\n  constructor(...args) {\n    super(...args);\n\n    _defineProperty(this, \"state\", {\n      search: \"\",\n      repo: \"\",\n      data: [],\n      filter: \"all\"\n    });\n\n    _defineProperty(this, \"updateUrl\", () => {\n      const Query = {\n        repo: this.state.repo,\n        filter: this.state.filter\n      };\n      this.props.history.push({\n        path: \"/\",\n        search: query_string__WEBPACK_IMPORTED_MODULE_2___default.a.stringify(Query)\n      });\n    });\n\n    _defineProperty(this, \"handleRepoSearch\", e => {\n      if (e.charCode == 13 || e.keyCode == 13) {\n        const repo = e.currentTarget.value;\n        this.props.fetchIssues(repo);\n        this.setState({\n          repo\n        }, () => this.updateUrl());\n      }\n    });\n\n    _defineProperty(this, \"handleInputChange\", e => {\n      this.setState({\n        search: e.currentTarget.value\n      });\n    });\n\n    _defineProperty(this, \"handleChangePageClick\", page => () => {\n      this.props.changePage(page);\n    });\n\n    _defineProperty(this, \"handleChangeFilter\", e => {\n      const {\n        name\n      } = e.currentTarget;\n      this.props.changeFilters(this.state.filter);\n      this.setState({\n        filter: name\n      }, () => {\n        //this.props.fetchIssues(this.state.repo);\n        this.updateUrl();\n      });\n    });\n  }\n\n  static fetching({\n    dispatch,\n    getState\n  }) {\n    return [dispatch(Object(_actions_issues__WEBPACK_IMPORTED_MODULE_3__[\"fetchIssues\"])(getState().repo))];\n  }\n\n  static getDerivedStateFromProps(props, state) {\n    if (props.issues.data) {\n      return _objectSpread({}, state, {\n        data: props.issues.data,\n        repo: props.issues.search,\n        search: state.search ? state.search : props.issues.search\n      });\n    }\n\n    return state;\n  }\n\n  componentDidMount() {\n    if (this.props.location) {\n      const query = query_string__WEBPACK_IMPORTED_MODULE_2___default.a.parse(this.props.location.search);\n      query.filter && this.setState({\n        filter: query.filter\n      }, () => this.props.changeFilters(query.filter));\n      query.repo && this.props.fetchIssues(query.repo);\n    }\n  }\n\n  componentWillUnmount() {\n    this.setState({\n      data: this.props.issues.data\n    });\n  }\n\n  render() {\n    const {\n      issues: {\n        isFetching,\n        links\n      }\n    } = this.props;\n    const {\n      data,\n      repo,\n      filter\n    } = this.state;\n    const pages = Object.keys(links);\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Header\"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Search\"], {\n      onKeyDown: this.handleRepoSearch,\n      onChange: this.handleInputChange,\n      value: this.state.search\n    }), isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Loading\"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Container\"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Container\"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Sidebar__WEBPACK_IMPORTED_MODULE_5__[\"default\"], {\n      handleChangeFilter: this.handleChangeFilter,\n      filter: filter\n    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Body\"], null, pages.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"Container\"], null, pages.map((page, index) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n      key: index,\n      onClick: this.handleChangePageClick(page)\n    }, page))), data.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__[\"IssuesList\"], null, data.map((item, i) => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ListItem__WEBPACK_IMPORTED_MODULE_6__[\"default\"], _extends({\n      key: i,\n      repo: repo\n    }, item))))))));\n  }\n\n}, _temp)) || _class);\n/* harmony default export */ __webpack_exports__[\"default\"] = (App);\n\n//# sourceURL=webpack:///./src/components/App/index.js?");

/***/ }),

/***/ "./src/components/IssueDetail/index.js":
/*!*********************************************!*\
  !*** ./src/components/IssueDetail/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react_markdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-markdown */ \"react-markdown\");\n/* harmony import */ var react_markdown__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_markdown__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ \"moment\");\n/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _actions_issues__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../actions/issues */ \"./src/actions/issues.js\");\n/* harmony import */ var _history__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../history */ \"./src/history.js\");\n/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../styles */ \"./src/components/styles.js\");\nvar _dec, _class;\n\n\n\n\n\n\n\n\n\nconst Header = styled_components__WEBPACK_IMPORTED_MODULE_3___default()(_styles__WEBPACK_IMPORTED_MODULE_7__[\"Container\"])`\n  justify-content: space-between;\n`;\nconst Issue = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.li`\n  display: flex;\n  flex-direction: column;\n  flex: 1;\n`;\nconst BackButton = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.button`\n  font-size: 1.8em;\n  padding: 5px 0;\n  width: 100px;\n  border-radius:10px;\n`;\nlet IssueDetail = (_dec = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__[\"connect\"])(({\n  issue,\n  issues\n}) => ({\n  issue,\n  issues\n}), {\n  fetchIssue: _actions_issues__WEBPACK_IMPORTED_MODULE_5__[\"fetchIssue\"]\n}), _dec(_class = class IssueDetail extends react__WEBPACK_IMPORTED_MODULE_0__[\"Component\"] {\n  static fetching({\n    dispatch,\n    path,\n    props\n  }) {\n    const parts = path.split(\"/\");\n    const user = parts[1];\n    const repo = parts[2];\n    parts.map((p, i) => console.log(\"p: \", i, p));\n    return [dispatch(Object(_actions_issues__WEBPACK_IMPORTED_MODULE_5__[\"fetchIssue\"])(user + \"/\" + repo + \"/issues/\" + parts[3]))];\n  }\n\n  componentDidMount() {\n    if (this.props.match) {\n      const {\n        user,\n        repo,\n        number\n      } = this.props.match.params;\n      this.props.fetchIssue(`${user}/${repo}/issues/${number}`);\n    }\n  }\n\n  render() {\n    const {\n      history,\n      issue: {\n        title,\n        body,\n        created_at\n      }\n    } = this.props;\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Issue, null, history.length > 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(BackButton, {\n      onClick: history.goBack\n    }, \"BACK\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Header, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h1\", null, title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h2\", null, moment__WEBPACK_IMPORTED_MODULE_4___default()(created_at).format())), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_markdown__WEBPACK_IMPORTED_MODULE_2___default.a, {\n      source: body\n    }));\n  }\n\n}) || _class);\n/* harmony default export */ __webpack_exports__[\"default\"] = (IssueDetail);\n\n//# sourceURL=webpack:///./src/components/IssueDetail/index.js?");

/***/ }),

/***/ "./src/components/ListItem/index.js":
/*!******************************************!*\
  !*** ./src/components/ListItem/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nconst Issue = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.li`\n  font-size: 1.2em;\n`;\n/* harmony default export */ __webpack_exports__[\"default\"] = (({\n  title,\n  number,\n  repo\n}) => {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Issue, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"NavLink\"], {\n    to: `/${repo}/${number}`\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h4\", null, title)));\n});\n\n//# sourceURL=webpack:///./src/components/ListItem/index.js?");

/***/ }),

/***/ "./src/components/Sidebar/index.js":
/*!*****************************************!*\
  !*** ./src/components/Sidebar/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles */ \"./src/components/styles.js\");\n\n\n\nconst ALL = \"all\";\nconst OPEN = \"open\";\nconst CLOSED = \"closed\";\nconst Input = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.input.attrs({\n  type: \"checkbox\"\n})`\n`;\n/* harmony default export */ __webpack_exports__[\"default\"] = (({\n  filter,\n  handleChangeFilter\n}) => {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__[\"Filters\"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h3\", null, \"Type\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Input, {\n    onChange: handleChangeFilter,\n    name: ALL,\n    checked: filter === ALL\n  }), \"All\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Input, {\n    onChange: handleChangeFilter,\n    name: OPEN,\n    checked: filter === OPEN\n  }), \"Open\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Input, {\n    onChange: handleChangeFilter,\n    name: CLOSED,\n    checked: filter === CLOSED\n  }), \"Closed\"));\n});\n\n//# sourceURL=webpack:///./src/components/Sidebar/index.js?");

/***/ }),

/***/ "./src/components/styles.js":
/*!**********************************!*\
  !*** ./src/components/styles.js ***!
  \**********************************/
/*! exports provided: Header, Loading, Search, Container, Body, Filters, IssuesList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Header\", function() { return Header; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Loading\", function() { return Loading; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Search\", function() { return Search; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Container\", function() { return Container; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Body\", function() { return Body; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Filters\", function() { return Filters; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"IssuesList\", function() { return IssuesList; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);\n\n\nconst Header = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div`\n    position: relative;\n`;\nconst Image = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.img`\n  display: block;\n  position: absolute;\n  top: .3em;\n  right: 2em;\n`;\nconst Loading = () => react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Image, {\n  src: \"../../img/loading.svg\",\n  width: \"30\",\n  alt: \"\"\n});\nconst Search = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.input`\n  width:100%;\n  display: flex;\n  background: #ededed url(https://static.tumblr.com/ftv85bp/MIXmud4tx/search-icon.png) no-repeat 9px center;\n  border: solid 1px #ccc;\n  padding: 9px 10px 9px 32px;\n  border-radius: 10em;\n  font-size:1.4em;\n  flex: 1;\n`;\nconst Container = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div`\n  display: flex;\n  flex: 1;\n  margin-top:1em;\n`;\nconst Body = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div`\n  display: flex;\n  flex: 1;\n  flex-direction: column;\n`;\nconst Filters = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div`\n  display: flex;\n  order: -1;\n  flex: 0 0 12em;\n  flex-direction: column;\n`;\nconst IssuesList = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.ul`\n  list-style: none;\n`;\n\n//# sourceURL=webpack:///./src/components/styles.js?");

/***/ }),

/***/ "./src/history.js":
/*!************************!*\
  !*** ./src/history.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! history */ \"history\");\n/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(history__WEBPACK_IMPORTED_MODULE_0__);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(history__WEBPACK_IMPORTED_MODULE_0__[\"createMemoryHistory\"])());\n\n//# sourceURL=webpack:///./src/history.js?");

/***/ }),

/***/ "./src/reducers/index.js":
/*!*******************************!*\
  !*** ./src/reducers/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _issues__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./issues */ \"./src/reducers/issues.js\");\n/* harmony import */ var _issue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./issue */ \"./src/reducers/issue.js\");\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(redux__WEBPACK_IMPORTED_MODULE_0__[\"combineReducers\"])({\n  issues: _issues__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  issue: _issue__WEBPACK_IMPORTED_MODULE_2__[\"default\"]\n}));\n\n//# sourceURL=webpack:///./src/reducers/index.js?");

/***/ }),

/***/ "./src/reducers/issue.js":
/*!*******************************!*\
  !*** ./src/reducers/issue.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/actionNames */ \"./src/actions/actionNames.js\");\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\nconst INITIAL_STATE = {\n  title: \"\",\n  body: \"\",\n  created_at: \"\",\n  isFetching: false,\n  lastUpdate: Date.now()\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = ((state = INITIAL_STATE, action) => {\n  switch (action.type) {\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"REQUEST_ISSUE\"]:\n      {\n        return _objectSpread({}, state, {\n          isFetching: true\n        });\n      }\n\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"RECEIVE_ISSUE\"]:\n      {\n        return _objectSpread({}, state, {\n          isFetching: false\n        }, action.payload);\n      }\n\n    default:\n      return state;\n  }\n});\n\n//# sourceURL=webpack:///./src/reducers/issue.js?");

/***/ }),

/***/ "./src/reducers/issues.js":
/*!********************************!*\
  !*** ./src/reducers/issues.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/actionNames */ \"./src/actions/actionNames.js\");\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\nconst INITIAL_STATE = {\n  data: [],\n  search: \"\",\n  isFetching: false,\n  perPage: 10,\n  page: 1,\n  filter: \"all\",\n  links: {},\n  lastUpdate: Date.now()\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = ((state = INITIAL_STATE, action) => {\n  switch (action.type) {\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"CHANGE_PAGE\"]:\n      {\n        return _objectSpread({}, state, {\n          page: action.payload\n        });\n      }\n\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"CHANGE_FILTERS\"]:\n      {\n        return _objectSpread({}, state, {\n          filter: action.payload\n        });\n      }\n\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"REQUEST_ISSUES\"]:\n      {\n        return _objectSpread({}, state, {\n          isFetching: true,\n          search: action.payload\n        });\n      }\n\n    case _actions_actionNames__WEBPACK_IMPORTED_MODULE_0__[\"RECEIVE_ISSUES\"]:\n      {\n        if (action.payload.headers) {\n          return _objectSpread({}, state, {\n            links: _objectSpread({}, parseLinks(action.payload.headers.link)),\n            isFetching: false,\n            data: action.payload.data\n          });\n        } else {\n          return state;\n        }\n      }\n\n    default:\n      return state;\n  }\n});\n\nfunction parseLinks(header) {\n  if (header.length == 0) {\n    throw new Error(\"input must not be of zero length\");\n  }\n\n  var parts = header.split(\",\");\n  var links = {};\n  parts.map(function (p) {\n    var section = p.split(\";\");\n\n    if (section.length != 2) {\n      throw new Error(\"section could not be split on ';'\");\n    }\n\n    var url = section[0].replace(/<(.*)>/, \"$1\").trim();\n    var name = section[1].replace(/rel=\"(.*)\"/, \"$1\").trim();\n    links[name] = url;\n  });\n  return links;\n}\n\n//# sourceURL=webpack:///./src/reducers/issues.js?");

/***/ }),

/***/ "./src/router/Routes.js":
/*!******************************!*\
  !*** ./src/router/Routes.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_App__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/App */ \"./src/components/App/index.js\");\n/* harmony import */ var _components_IssueDetail__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/IssueDetail */ \"./src/components/IssueDetail/index.js\");\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ([{\n  component: _components_App__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  path: \"/\",\n  exact: true\n}, {\n  component: _components_IssueDetail__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  path: \"/:user/:repo/:number\"\n}]);\n\n//# sourceURL=webpack:///./src/router/Routes.js?");

/***/ }),

/***/ "./src/store.js":
/*!**********************!*\
  !*** ./src/store.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _reducers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reducers */ \"./src/reducers/index.js\");\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-thunk */ \"redux-thunk\");\n/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(redux__WEBPACK_IMPORTED_MODULE_0__[\"createStore\"])(_reducers__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {}, Object(redux__WEBPACK_IMPORTED_MODULE_0__[\"applyMiddleware\"])(redux_thunk__WEBPACK_IMPORTED_MODULE_2___default.a)));\n\n//# sourceURL=webpack:///./src/store.js?");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"axios\");\n\n//# sourceURL=webpack:///external_%22axios%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "history":
/*!**************************!*\
  !*** external "history" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"history\");\n\n//# sourceURL=webpack:///external_%22history%22?");

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"moment\");\n\n//# sourceURL=webpack:///external_%22moment%22?");

/***/ }),

/***/ "query-string":
/*!*******************************!*\
  !*** external "query-string" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"query-string\");\n\n//# sourceURL=webpack:///external_%22query-string%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-markdown":
/*!*********************************!*\
  !*** external "react-markdown" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-markdown\");\n\n//# sourceURL=webpack:///external_%22react-markdown%22?");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-redux\");\n\n//# sourceURL=webpack:///external_%22react-redux%22?");

/***/ }),

/***/ "react-router-config":
/*!**************************************!*\
  !*** external "react-router-config" ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router-config\");\n\n//# sourceURL=webpack:///external_%22react-router-config%22?");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router-dom\");\n\n//# sourceURL=webpack:///external_%22react-router-dom%22?");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"redux\");\n\n//# sourceURL=webpack:///external_%22redux%22?");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"redux-thunk\");\n\n//# sourceURL=webpack:///external_%22redux-thunk%22?");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"styled-components\");\n\n//# sourceURL=webpack:///external_%22styled-components%22?");

/***/ })

/******/ });