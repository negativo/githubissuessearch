const path = require("path");
const webpackNodeExternals = require("webpack-node-externals");
console.log(path.resolve(__dirname + "./server"));

module.exports = {
  mode: "development",
  target: "node",
  entry: "./src/server/index.js",
  output: {
    path: path.resolve(__dirname + "/server"),
    filename: "server.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/
      }
    ]
  },
  externals: [webpackNodeExternals()]
};
