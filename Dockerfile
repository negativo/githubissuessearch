FROM node:carbon

WORKDIR /usr/src/app

COPY package*.json ./
COPY . .

RUN ls
RUN set -x \
  && npm install\
  && npm run build:client\
  && npm run build:server


EXPOSE 8079

CMD [ "node", "server/server" ]
