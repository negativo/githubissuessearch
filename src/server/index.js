import express from "express";
import { matchRoutes } from "react-router-config";
import render from "./render";
import store from "../store";
import Routes from "../router/Routes";

const PORT = process.env.PORT || 8079;
const app = express();

app.use("/js", express.static("server/public/js"));
app.use("/img", express.static("server/public/img"));
app.get("/*", async (req, res) => {
  const actions = matchRoutes(Routes, req.path)
    .map(({ route }) =>
      route.component.fetching
        ? route.component.fetching({ ...store, path: req.path, req })
        : null
    )
    .map(
      async actions =>
        await Promise.all(
          (actions || []).map(
            p => p && new Promise(resolve => p.then(resolve).catch(resolve))
          )
        )
    );

  await Promise.all(actions);
  const context = {};
  const content = render(req.url, store, context);

  res.send(content);
});

app.listen(PORT, () =>
  console.log(`Frontend service listening on port: ${PORT}`)
);
