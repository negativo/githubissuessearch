import React from "react";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import Routes from "../router/Routes";
import history from "../history";

export default (pathname, store, context) => {
  const content = renderToString(
    <Provider store={store}>
      <Router history={history} location={pathname} context={context}>
        <div>{renderRoutes(Routes)}</div>
      </Router>
    </Provider>
  );

  return `
  <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <title>Title</title>
      </head>
      <body>
      
      <div id="app">${content}</div>
      <script>
        window.INITIAL_STATE = ${JSON.stringify(store.getState())}
      </script>
      <script src="/js/bundle.js"></script>
      </body>
      </html>
  `;
};
