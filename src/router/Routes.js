import App from "../components/App";
import IssueDetail from "../components/IssueDetail";

export default [
  {
    component: App,
    path: "/",
    exact: true
  },
  {
    component: IssueDetail,
    path: "/:user/:repo/:number"
  }
];
