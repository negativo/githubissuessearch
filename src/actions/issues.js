import axios from "axios";

import {
  ROOT,
  CHANGE_FILTER,
  CHANGE_PAGE,
  REQUEST_ISSUES,
  RECEIVE_ISSUES,
  REQUEST_ISSUE,
  RECEIVE_ISSUE
} from "./actionNames";

export const changePage = page => async (dispatch, getState) => {
  try {
    const res = await axios.get(getState().issues.links[page]);
    dispatch({ type: RECEIVE_ISSUES, payload: res });
    dispatch({ type: CHANGE_PAGE, payload: page });
  } catch (e) {
    dispatch({ type: RECEIVE_ISSUES, payload: [] });
  }
};

export const fetchIssues = name => async (dispatch, getState) => {
  const { filter, perPage, page } = getState().issues;
  if (name) {
    try {
      dispatch({ type: REQUEST_ISSUES, payload: name });
      const res = await axios.get(
        `${ROOT}${name}/issues?state=${filter}&per_page=${perPage}&page=${page}`
      );
      dispatch({ type: RECEIVE_ISSUES, payload: res });
    } catch (e) {
      dispatch({ type: RECEIVE_ISSUES, payload: [] });
    }
  }
};

export const fetchIssue = path => async dispatch => {
  try {
    dispatch({ type: REQUEST_ISSUE });
    const res = await axios.get(`${ROOT}${path}`);
    dispatch({ type: RECEIVE_ISSUE, payload: res.data });
  } catch (e) {
    dispatch({ type: RECEIVE_ISSUE, payload: {} });
  }
};

export const changeFilter = filter => async (dispatch, getState) => {
  dispatch({ type: CHANGE_FILTER, payload: filter });
};
