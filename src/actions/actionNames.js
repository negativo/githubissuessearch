export const ROOT = "https://api.github.com/repos/";

export const CHANGE_FILTER = "CHANGE_FILTER";

export const CHANGE_PAGE = "CHANGE_PAGE";

export const REQUEST_ISSUES = "REQUEST_ISSUES";
export const RECEIVE_ISSUES = "RECEIVE_ISSUES";

export const REQUEST_ISSUE = "REQUEST_ISSUE";
export const RECEIVE_ISSUE = "RECEIVE_ISSUE";
