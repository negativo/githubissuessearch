import React from "react";
import { hydrate } from "react-dom";
import Router from "./router";
import { Provider } from "react-redux";
import store from "./store";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  body {
    padding: 0;
    margin: 0;
    font-family: sans-serif;
    font-size: 10px;
    padding: 10px;
  }
  * {
    box-sizing: border-box;
  }
  #app{
    margin:20px auto;
    max-width:1240px;
  }
`;

hydrate(
  <React.Fragment>
    <GlobalStyle />
    <Provider store={store}>
      <Router />
    </Provider>
  </React.Fragment>,
  document.querySelector("#app")
);
