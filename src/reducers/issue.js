import { REQUEST_ISSUE, RECEIVE_ISSUE } from "../actions/actionNames";

const INITIAL_STATE = {
  title: "",
  body: "",
  created_at: "",
  isFetching: false,
  lastUpdate: Date.now()
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_ISSUE: {
      return { ...state, isFetching: true };
    }
    case RECEIVE_ISSUE: {
      return { ...state, isFetching: false, ...action.payload };
    }
    default:
      return state;
  }
};
