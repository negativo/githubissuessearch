import {
  REQUEST_ISSUES,
  RECEIVE_ISSUES,
  CHANGE_FILTER,
  CHANGE_PAGE
} from "../actions/actionNames";

const INITIAL_STATE = {
  data: [],
  search: "",
  isFetching: false,
  perPage: 10,
  page: 1,
  filter: "all",
  links: {},
  lastUpdate: Date.now()
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_PAGE: {
      return {
        ...state,
        page: action.payload
      };
    }
    case CHANGE_FILTER: {
      return {
        ...state,
        filter: action.payload
      };
    }
    case REQUEST_ISSUES: {
      return { ...state, isFetching: true, search: action.payload };
    }
    case RECEIVE_ISSUES: {
      if (action.payload.headers) {
        return {
          ...state,
          links: { ...parseLinks(action.payload.headers.link) },
          isFetching: false,
          data: action.payload.data
        };
      } else {
        return state;
      }
    }
    default:
      return state;
  }
};

function parseLinks(header) {
  if (header.length == 0) {
    throw new Error("input must not be of zero length");
  }

  var parts = header.split(",");
  var links = {};

  parts.map(function(p) {
    var section = p.split(";");
    if (section.length != 2) {
      throw new Error("section could not be split on ';'");
    }
    var url = section[0].replace(/<(.*)>/, "$1").trim();
    var name = section[1].replace(/rel="(.*)"/, "$1").trim();
    links[name] = url;
  });

  return links;
}
