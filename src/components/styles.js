import React from "react";
import Styled from "styled-components";

export const Header = Styled.div`
    position: relative;
`;

const Image = Styled.img`
  display: block;
  position: absolute;
  top: .3em;
  right: 2em;
`;

export const Loading = () => <Image src="/img/loading.svg" width="30" alt="" />;

export const Search = Styled.input`
  width:100%;
  display: flex;
  background: #ededed url(https://static.tumblr.com/ftv85bp/MIXmud4tx/search-icon.png) no-repeat 9px center;
  border: solid 1px #ccc;
  padding: 9px 10px 9px 32px;
  border-radius: 10em;
  font-size:1.4em;
  flex: 1;
`;

export const Container = Styled.div`
  display: flex;
  flex: 1;
  margin-top:1em;
`;

export const Body = Styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export const Filters = Styled.div`
  display: flex;
  order: -1;
  flex: 0 0 12em;
  flex-direction: column;
`;

export const IssuesList = Styled.ul`
  list-style: none;
`;
