import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import ListItem from "./";

it("ListItem renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <ListItem title="test" number="123456" repo="test/repo" />
    </Router>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
