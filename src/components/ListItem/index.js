//@flow
import React from "react";
import { NavLink } from "react-router-dom";
import Styled from "styled-components";

const Issue = Styled.li`
  font-size: 1.2em;
`;

type Props = {
  title: string,
  number: string,
  repo: string
};

export default ({ title, number, repo }: Props) => {
  return (
    <Issue>
      <NavLink to={`/${repo}/${number}`}>
        <h4>{title}</h4>
      </NavLink>
    </Issue>
  );
};
