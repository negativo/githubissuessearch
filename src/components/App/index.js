//@flow
import React, { Component } from "react";
import { connect } from "react-redux";
import qs from "query-string";
import { fetchIssues, changeFilter, changePage } from "../../actions/issues";
import {
  Header,
  Loading,
  Search,
  Container,
  Body,
  IssuesList
} from "../styles";
import Sidebar from "../Sidebar";
import ListItem from "../ListItem";
import Pages from "../Pages";

type Props = {
  issues: {
    data: [],
    search: string,
    isFetching: boolean,
    links: {}
  },
  location: {
    search: string
  },
  fetchIssues: string => {},
  changeFilter: string => {},
  changePage: string => {}
};

type State = {
  search: string,
  repo: string,
  data: [],
  filter: string
};

type Fetching = {
  dispatch: string => ({}) => {},
  getState: () => { repo: string }
};

@connect(
  ({ issues }) => ({ issues }),
  { fetchIssues, changeFilter, changePage }
)
class App extends Component<Props, State> {
  static fetching({ dispatch, getState, req }: Fetching) {
    return [dispatch(fetchIssues(getState().repo))];
  }

  state = {
    search: "",
    repo: "",
    data: [],
    filter: "all"
  };

  static getDerivedStateFromProps(props: Props, state: State) {
    if (props.issues.data) {
      return {
        ...state,
        data: props.issues.data,
        repo: props.issues.search,
        search: state.search ? state.search : props.issues.search
      };
    }
    return state;
  }

  componentDidMount() {
    if (this.props.location) {
      const query = qs.parse(this.props.location.search);
      query.filter &&
        this.setState(
          {
            filter: query.filter
          },
          () => this.props.changeFilter(query.filter)
        );

      query.repo && this.props.fetchIssues(query.repo);
    }
  }

  updateUrl = () => {
    const Query = {
      repo: this.state.repo,
      filter: this.state.filter
    };

    this.props.history.push({ path: "/", search: qs.stringify(Query) });
  };

  handleRepoSearch = (e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    if (e.charCode == 13 || e.keyCode == 13) {
      const repo = e.currentTarget.value;
      this.props.fetchIssues(repo);
      this.setState({ repo }, () => this.updateUrl());
    }
  };

  handleInputChange = (e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    this.setState({ search: e.currentTarget.value });
  };

  handleChangePageClick = (page: string) => () => {
    this.props.changePage(page);
  };

  handleChangeFilter = (e: SyntheticEvent<HTMLButtonElement>) => {
    const { name } = e.currentTarget;

    this.props.changeFilter(name);
    this.setState(
      {
        filter: name
      },
      () => {
        this.props.fetchIssues(this.state.repo);
        this.updateUrl();
      }
    );
  };

  componentWillUnmount() {
    this.setState({
      data: this.props.issues.data
    });
  }

  render() {
    const {
      issues: { isFetching, links }
    } = this.props;
    const { data, repo, filter } = this.state;
    const pages = Object.keys(links);

    return (
      <React.Fragment>
        <Header>
          <Search
            onKeyDown={this.handleRepoSearch}
            onChange={this.handleInputChange}
            value={this.state.search}
            placeholder="Search for a Github Repo (facebook/react) and press enter"
          />
          {isFetching && <Loading />}
        </Header>
        <Container>
          <Sidebar
            handleChangeFilter={this.handleChangeFilter}
            filter={filter}
          />
          <Body>
            <Pages data={pages} onItemClick={this.handleChangePageClick} />
            {data.length > 0 && (
              <IssuesList>
                {data.map((item, i) => (
                  <ListItem key={i} repo={repo} {...item} />
                ))}
              </IssuesList>
            )}
          </Body>
        </Container>
      </React.Fragment>
    );
  }
}
export default App;
