import React from "react";
import ReactDOM from "react-dom";

import Sidebar from "./";

it("Sidebar renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Sidebar filter="all" handleChangeFilter={() => {}} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
