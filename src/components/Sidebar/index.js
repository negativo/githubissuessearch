//@flow
import React from "react";
import Styled from "styled-components";
import { Filters } from "../styles";
const ALL = "all";
const OPEN = "open";
const CLOSED = "closed";

const Input = Styled.input.attrs({ type: "checkbox" })`
`;

type Props = {
  filter: string,
  handleChangeFilter: (SyntheticEvent<HTMLButtonElement>) => void
};

export default ({ filter, handleChangeFilter }: Props) => {
  return (
    <Filters>
      <h3>Type</h3>
      <div>
        <Input
          onChange={handleChangeFilter}
          name={ALL}
          checked={filter === ALL}
        />
        All
      </div>
      <div>
        <Input
          onChange={handleChangeFilter}
          name={OPEN}
          checked={filter === OPEN}
        />
        Open
      </div>
      <div>
        <Input
          onChange={handleChangeFilter}
          name={CLOSED}
          checked={filter === CLOSED}
        />
        Closed
      </div>
    </Filters>
  );
};
