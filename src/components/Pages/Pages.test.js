import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import Pages from "./";

it("Pages renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Router>
      <Pages data={[{ page: "url", onItemClick={()=>{}}}]} />
    </Router>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
