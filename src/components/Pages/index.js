//@flow
import React from "react";
import Styled from "styled-components";

const Container = Styled.div`
  display:flex;
  flex-direction: row-reverse;
`;

type Props = {
  data: [],
  onItemClick: () => {}
};

export default ({ data, onItemClick }: Props) => {
  if (!data) {
    return null;
  }
  return (
    data.length > 0 && (
      <Container>
        {data.map((page, index) => (
          <button key={index} onClick={onItemClick(page)}>
            {page}
          </button>
        ))}
      </Container>
    )
  );
};
