import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "../../store";
import IssueDetail from "./";

it("IssueDetail renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <IssueDetail />
    </Provider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
