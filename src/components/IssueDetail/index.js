//@flow
import React, { Component } from "react";
import { connect } from "react-redux";
import ReactMarkdown from "react-markdown";
import Styled from "styled-components";
import Moment from "moment";
import { fetchIssue } from "../../actions/issues";
import history from "../../history";
import { Container } from "../styles";

const Header = Styled(Container)`
  justify-content: space-between;
`;

const Issue = Styled.li`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const BackButton = Styled.button`
  font-size: 1.8em;
  padding: 5px 0;
  width: 100px;
  border-radius:10px;
`;

type Props = {
  match: {
    params: {
      user: string,
      repo: string,
      number: number
    }
  },
  history: {},
  issue: {
    title: string,
    body: string,
    created_at: string
  },
  fetchIssue: string => {}
};

type Fetching = {
  dispatch: string => ({}) => {},
  path: string,
  props: {
    issues: {
      user: string,
      repo: string
    }
  }
};

@connect(
  ({ issue, issues }) => ({ issue, issues }),
  { fetchIssue: fetchIssue }
)
class IssueDetail extends Component<Props> {
  static fetching({ dispatch, path, props }: Fetching) {
      
      const parts = path.split("/");
      const user = parts[1];
      const repo = parts[2];
      const number = parts[2];
      
      return [
        dispatch(fetchIssue(`${user}/${repo}/issues/${number}`))
      ];

  }
  componentDidMount() {
    if (this.props.match) {
      const { user, repo, number } = this.props.match.params;
      this.props.fetchIssue(`${user}/${repo}/issues/${number}`);
    }
  }

  render() {
    const {
      history,
      issue: { title, body, created_at }
    } = this.props;

    return (
      <Issue>
        {history.action === "PUSH" && (
          <BackButton onClick={history.goBack}>BACK</BackButton>
        )}
        <Header>
          <h1>{title}</h1>
          <h2>{Moment(created_at).fromNow()}</h2>
        </Header>
        <ReactMarkdown source={body} />
      </Issue>
    );
  }
}
export default IssueDetail;
