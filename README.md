## Run using docker

```bash
$ docker-compose up
```

Then navigate to localhost:8079

## Run the frontend app only

```bash
$ npm install
$ npm run start
```

## Build and run server

```bash
$ npm install
$ npm run build:client
$ npm run build:server
$ node server.js
```

Then navigate to localhost:8079
